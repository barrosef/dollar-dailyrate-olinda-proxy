package coopersystem.recruit.architectchallenge.context;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
public class ArchitectChallengConfiguration {

    @Value("${app.olinda-url}")
    private String olindaURL;

    @Value("${app.olinda-entity}")
    private String olindaEntity;
}
