package coopersystem.recruit.architectchallenge.dollarratedaily.service.proxy;

import coopersystem.recruit.architectchallenge.context.ArchitectChallengConfiguration;
import coopersystem.recruit.architectchallenge.dollarratedaily.model.DollarRateDaily;
import coopersystem.recruit.architectchallenge.dollarratedaily.model.DollarRateDailyDTO;
import org.apache.olingo.client.api.ODataClient;
import org.apache.olingo.client.api.communication.request.retrieve.ODataEntityRequest;
import org.apache.olingo.client.api.communication.response.ODataRetrieveResponse;
import org.apache.olingo.client.api.domain.ClientEntity;
import org.apache.olingo.client.api.uri.URIBuilder;
import org.apache.olingo.client.core.ODataClientFactory;
import org.apache.olingo.commons.api.format.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ReflectionUtils;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;

@Component
public class BCBOlindaProxyImpl implements BCBOlindaProxy {

    private final ODataClient olindaClient;
    private ArchitectChallengConfiguration configuration;

    @Autowired
    BCBOlindaProxyImpl(ArchitectChallengConfiguration configuration) {
        this.olindaClient = ODataClientFactory.getClient();
        this.configuration = configuration;
        olindaClient.getConfiguration().setDefaultPubFormat(ContentType.APPLICATION_JSON);
    }

    public DollarRateDaily findDollarRateByDate(DollarRateDailyDTO dollarRateDailyDTO) {

        DollarRateDaily dollarRateDaily = null;

        URIBuilder uriBuilder = olindaClient.newURIBuilder(configuration.getOlindaURL());
        uriBuilder.appendEntityIdSegment(configuration.getOlindaEntity());
        uriBuilder.filter(dollarRateDailyDTO.getFilter());
        uriBuilder.skip((int) dollarRateDailyDTO.getOffset());
        uriBuilder.top(dollarRateDailyDTO.getPageSize() * (int) dollarRateDailyDTO.getOffset());
        dollarRateDailyDTO.getSort().stream().map(order ->
            uriBuilder.orderBy(new StringBuilder(order.getProperty()).append(order.getDirection().name()).toString())
        );

        ODataEntityRequest<ClientEntity> request = olindaClient.getRetrieveRequestFactory()
                .getEntityRequest(uriBuilder.build());
        final ODataRetrieveResponse<ClientEntity> response = request.execute();

        if (response.getStatusCode() == 200) {
            ClientEntity entity = response.getBody();
            dollarRateDaily = new DollarRateDaily();
            DollarRateDaily finalDollarRateDaily = dollarRateDaily;
            dollarRateDailyDTO.getProperties().stream().filter(property -> dollarRateDailyDTO.hasProperty(property))
                .forEach(property -> {
                    Field reflectField = ReflectionUtils.findField(DollarRateDaily.class, property);
                    ReflectionUtils.setField(reflectField, finalDollarRateDaily, entity.getProperty(property).getValue());
                }
            );
        }

        return dollarRateDaily;
    }
}