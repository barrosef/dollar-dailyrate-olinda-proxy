package coopersystem.recruit.architectchallenge.dollarratedaily.service.proxy;

import coopersystem.recruit.architectchallenge.dollarratedaily.model.DollarRateDaily;
import coopersystem.recruit.architectchallenge.dollarratedaily.model.DollarRateDailyDTO;

public interface BCBOlindaProxy {

    DollarRateDaily findDollarRateByDate(DollarRateDailyDTO dollarRateDailyDTO);
}