package coopersystem.recruit.architectchallenge.dollarratedaily.service;

import coopersystem.recruit.architectchallenge.dollarratedaily.model.DollarRateDailyDTO;
import org.springframework.context.ApplicationEvent;

/**
 * Evento disparado quando uma query à api olinda é realizada.
 */
public class QueryRateEvent extends ApplicationEvent {

    public QueryRateEvent(DollarRateDailyDTO source) {
        super(source);
    }
}
