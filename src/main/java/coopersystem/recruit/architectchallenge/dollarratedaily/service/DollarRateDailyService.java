package coopersystem.recruit.architectchallenge.dollarratedaily.service;

import coopersystem.recruit.architectchallenge.dollarratedaily.model.DollarRateDaily;
import coopersystem.recruit.architectchallenge.dollarratedaily.model.DollarRateDailyDTO;
import coopersystem.recruit.architectchallenge.dollarratedaily.service.proxy.BCBOlindaProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

@Service
public class DollarRateDailyService {

    private final ApplicationEventPublisher publisher;

    @Autowired
    private BCBOlindaProxy proxy;


    @Autowired
    public DollarRateDailyService(ApplicationEventPublisher publisher) {
        this.publisher = publisher;
    }

    public DollarRateDaily findDollarRateByDate(DollarRateDailyDTO dollarRateDailyDTO) {
        DollarRateDaily dollarRateDaily = proxy.findDollarRateByDate(dollarRateDailyDTO);
        dollarRateDailyDTO.setDailyRate(dollarRateDaily);
        publisher.publishEvent(new QueryRateEvent(dollarRateDailyDTO));
        return dollarRateDaily;
    }
}
