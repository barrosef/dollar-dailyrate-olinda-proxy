package coopersystem.recruit.architectchallenge.dollarratedaily.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * Entidade para cotação de dólar em data específica
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DollarRateDaily {
    private float cotacaoCompra;
    private float cotacaoVenda;
    private LocalDateTime dataHoraCotacao;
}
