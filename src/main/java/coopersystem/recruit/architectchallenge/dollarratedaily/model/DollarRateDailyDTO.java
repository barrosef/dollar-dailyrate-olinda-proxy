package coopersystem.recruit.architectchallenge.dollarratedaily.model;

import lombok.Data;
import lombok.experimental.Delegate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * DTO para transporte de dados complexos que chegam an API para o proxy de integração com o BCB, armazena os dados
 * de pesquisa
 * @author barrosef@gmail.com
 */
@Data
public class DollarRateDailyDTO {

    /**
     * Data da cotação, atributo obrigatório na API olinda do BCB
     */
    private LocalDate dateRate;

    /**
     * Objeto do spring framework que armazena os dados de índices de paginação
     */
    @Delegate
    private Pageable pageable;

    /**
     * Propriedade filter, armazena as condições de filtro para requisição na olinda - oData API do BCB
     */
    private String filter;

    /**
     * Propriedades (campos) identificam os campos que deve ser retornados da olinda - oData API do BCB
     */
    private Set<String> properties;

    /**
     * Timestamp da requisição, objeto criado pelo microcontainer no momento da requisição
     */
    private LocalDateTime dateTime;

    /**
     * Cotação de dólar encontrada para o dia informado
     */
    private DollarRateDaily dailyRate;


    public DollarRateDailyDTO() {
        this.dailyRate = new DollarRateDaily();
        pageable = Page.empty().getPageable();
        properties = new LinkedHashSet();
        dateTime = LocalDateTime.now();
    }

    /**
     * Verifica se uma determinada propriedade existe nas propriedades selecionadas para retornar da olinda - oData API
     * do BCP.
     *
     * @param propertyName nome da propriedade
     * @return true se a propriedade foi selecionada senão, false.
     */
    public boolean hasProperty(String propertyName) {
        return this.properties.contains(propertyName);
    }
}
