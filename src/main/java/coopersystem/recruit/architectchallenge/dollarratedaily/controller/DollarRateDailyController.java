package coopersystem.recruit.architectchallenge.dollarratedaily.controller;

import coopersystem.recruit.architectchallenge.dollarratedaily.service.DollarRateDailyService;
import coopersystem.recruit.architectchallenge.dollarratedaily.model.DollarRateDaily;
import coopersystem.recruit.architectchallenge.dollarratedaily.model.DollarRateDailyDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DollarRateDailyController {

    @Autowired
    DollarRateDailyService service;

    @RequestMapping(path = "findDoolarRateByDate", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public DollarRateDailyDTO findDollarRateByDate(@RequestBody DollarRateDailyDTO dollarRateDailyDTO) {
        DollarRateDaily dollarRateDaily = service.findDollarRateByDate(dollarRateDailyDTO);
        dollarRateDailyDTO.setDailyRate(dollarRateDaily);
        return dollarRateDailyDTO;
    }
}
