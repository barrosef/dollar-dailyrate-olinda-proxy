package coopersystem.recruit.architectchallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArchitectChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArchitectChallengeApplication.class, args);
	}

}
