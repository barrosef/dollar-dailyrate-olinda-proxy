package coopersystem.recruit.architectchallenge;

import coopersystem.recruit.architectchallenge.dollarratedaily.model.DollarRateDailyDTO;
import coopersystem.recruit.architectchallenge.dollarratedaily.service.proxy.BCBOlindaProxy;
import coopersystem.recruit.architectchallenge.dollarratedaily.service.proxy.BCBOlindaProxyImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BCBOlindaProxyTest {

    @MockBean
    private BCBOlindaProxyImpl BCBOlindaProxy;

    @Test
    public void findDollarRateByDateTest() {
        DollarRateDailyDTO dto = new DollarRateDailyDTO();
        dto.setDateRate(LocalDate.of(2021, 9, 15));
        dto.setFilter("dataCotacao eq 20210-9-9");
        dto.setPageable(PageRequest.of(1, 5));
        BCBOlindaProxy.findDollarRateByDate(dto);
    }

}
