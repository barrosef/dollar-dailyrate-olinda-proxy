FROM maven:3.6.0-jdk-11-slim AS builder
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package

FROM openjdk:11.0.2-jre-slim
ENV LANG pt_BR.UTF-8
ENV LANGUAGE pt_BR:pt
ENV LC_ALL pt_BR.UTF-8
COPY --from=builder /home/app/target/architect-challenge-0.0.1-SNAPSHOT.jar /app/app.jar
WORKDIR /app
CMD java -jar /app/app.jar
HEALTHCHECK --interval=5s --timeout=3s CMD curl --fail http://localhost:8080/actuator/healthcheck || exit 1
EXPOSE 8080/tcp